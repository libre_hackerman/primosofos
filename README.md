# Primosofos

Primosofos is a primes number calculator written in C that uses POSIX threads and calculates using the square root method.

### Usage

The program takes three arguments:
- **Number of threads**: how many threads should be calculating primes at the same time.
- **Chunk size**: each of the threads checks a *chunk* of numbers, this parameter allows to set the size of those chunks (the bigger the chunk, the more numbers it will check before printing the result and starting a new thread).
- **Maximum number** *(optional)*: if specified, the program will stop once this number is reached. Note that the program won't stop right at that number, it may calculate some more depending on the number of threads and the size of the chunks.

### Build
Build with `make`. That will leave you the `primosofos` executable.

### Installation
You can directly run the compiled executable, but if you want to install it
on your system, `make install` will do it. Notice that you can set the
variable `PREFIX` (default to /usr/local) to your desire.

### Uninstall
`make uninstall` (Mind the `PREFIX` too)
