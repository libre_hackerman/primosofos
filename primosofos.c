/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <locale.h>

typedef unsigned long long bigint_t;

struct args_s
{
	unsigned long n_threads;
	unsigned long chunk_size;
	bigint_t max;
};

struct chunk_s
{
	bigint_t start;
	unsigned long chunk_size;
	unsigned long n_primes;
	bigint_t *primes;
};

static bool parse_args(int argc, char *argv[], struct args_s *args)
{
	if (argc != 3 && argc != 4)
		return (false);
	for (int i = 1; i < argc; i++)
		if (!isdigit(argv[i][0]))
			return (false);
	args->n_threads = strtoul(argv[1], NULL, 10);
	args->chunk_size = strtoul(argv[2], NULL, 10);
	if (argc == 4)
		args->max = strtoull(argv[3], NULL, 10);
	else
		args->max = 0;
	return (true);
}

static struct chunk_s *init_chunks(const struct args_s *args)
{
	struct chunk_s *chunks;

	chunks = calloc(args->n_threads, sizeof(struct chunk_s));
	for (unsigned long i = 0; i < args->n_threads; i++)
	{
		chunks[i].start = args->chunk_size * i;
		chunks[i].chunk_size = args->chunk_size;
		chunks[i].n_primes = 0;
		chunks[i].primes = calloc(args->chunk_size, sizeof(bigint_t));
	}
	return (chunks);
}

static void delete_chunks(struct chunk_s *chunks, unsigned long n_chunks)
{
	for (unsigned long i = 0; i < n_chunks; i++)
		free(chunks[i].primes);
	free(chunks);
}

static bool is_prime(bigint_t n)
{
	bigint_t square = sqrtl(n);

	for (bigint_t i = 3; i <= square; i += 2ULL)
		if (n % i == 0)
			return (false);
	return (true);
}

static void *calculate_chunk(void *v_chunk)
{
	struct chunk_s *chunk = v_chunk;
	bigint_t start;
	bigint_t max;

	start = chunk->start % 2 == 0 ? chunk->start + 1 : chunk->start;
	max = chunk->start + chunk->chunk_size;
	for (bigint_t n = start; n < max; n += 2ULL)
		if (is_prime(n))
			chunk->primes[chunk->n_primes++] = n;
	return (NULL);
}

static void display_chunk(const struct chunk_s *chunk)
{
	for (unsigned long i = 0; i < chunk->n_primes; i++)
		printf("%'llu\n", chunk->primes[i]);
}

int main(int argc, char *argv[])
{
	struct args_s args;
	pthread_t *threads;
	struct chunk_s *chunks;
	bool keep_crunching;

	setlocale(LC_NUMERIC, "");

	if (!parse_args(argc, argv, &args))
	{
		fprintf(stderr, "USAGE: %s NUMBER_THREADS CHUNK_SIZE [MAX PRIME]\n",
				argv[0]);
		return (EXIT_FAILURE);
	}

	threads = calloc(args.n_threads, sizeof(pthread_t));
	chunks = init_chunks(&args);

	keep_crunching = true;
	while (args.max == 0 || keep_crunching)
	{
		for (unsigned long i = 0; i < args.n_threads; i++)
			pthread_create(threads + i, NULL, calculate_chunk, chunks + i);
		for (unsigned long i = 0; i < args.n_threads; i++)
		{
			pthread_join(threads[i], NULL);
			display_chunk(chunks + i);
			if (chunks[i].primes[chunks[i].n_primes - 1] >= args.max)
				keep_crunching = false;
			chunks[i].start += args.n_threads * args.chunk_size;
			chunks[i].n_primes = 0;
		}
	}

	free(threads);
	delete_chunks(chunks, args.n_threads);
	return (EXIT_SUCCESS);
}
