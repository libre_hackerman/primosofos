CC = cc
SHELL = /bin/sh
CFLAGS += -Wall -Werror -Wextra -O2
LDFLAGS += -lm -lpthread
PREFIX = /usr/local

NAME = primosofos

SRCS:= primosofos.c

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LDFLAGS)

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean $(NAME)

install: $(NAME)
	cp $(NAME) $(PREFIX)/bin
	chmod +x $(PREFIX)/bin/$(NAME)

uninstall:
	rm $(PREFIX)/bin/$(NAME)

debug: CFLAGS = -Wall -Werror -Wextra -g
debug: fclean $(NAME)

.PHONY: all clean fclean re install uninstall debug
